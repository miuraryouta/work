using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public float speed = 3.0f; //歩いているときの速度
    public float speed2 = 10.0f; //飛んでいるときの速度

    public bool hantei = false;

    public GameObject Cube;

    private Rigidbody rb; //リジッドボディを取得するための変数
    private float upForce = 5.0f; //上方向にかける力
    private bool isGround; //着地しているかどうかの判定

    private float countup = 0.0f;    //カウントアップ

    public float timelimit = 5.0f;    //タイムリミット

    //Enemyのステータス
    private int Hp = 100;

    //乱数
    int rnd;

    int flag = 0;  //判定用

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>(); //リジッドボディを取得
    }

    // Update is called once per frame
    void Update()
    {
        if(flag == 0)
        {
            rnd = Random.Range(1, 10); //1〜9の範囲でランダムな整数値が返る
        }

        

        if (isGround == true)
        {
            upForce = 0;
        }
        else if(isGround == false)
        {

        }

        //テスト
        //if(Input.GetKey("space"))
        //{
            //isGround = false;
            //upForce += 5 * Time.deltaTime;
            //rb.AddForce(new Vector3(0, upForce, 0));
        //}

        if(rnd == 1 && rnd == 2 && rnd == 3)
        {
            flag = 1;


        }

        //Hpが0になったとき
        if(Hp == 0)
        {
            //Enemyが死ぬ
            Destroy(gameObject);
        }

        

        if (rnd == 1)
        {
            //Destroy(gameObject);
        }

        if(hantei == true)
        {
            this.transform.LookAt(Cube.transform);
            if(isGround == true)
            {
                this.transform.Translate(0.0f, 0.0f, speed * Time.deltaTime);
            }
            else if(isGround == false)
            {
                this.transform.Translate(0.0f, 0.0f, speed2 * Time.deltaTime);
            }
            
        }

        if(flag == 1)
        {
            countup += Time.deltaTime;   //時間をカウントする

            isGround = false;
            upForce += 5 * Time.deltaTime;
            rb.AddForce(new Vector3(0, upForce, 0));
        }

        

        if(countup >= timelimit && flag == 1)
        {
            //何かしらの処理
            flag -= 1;
        }

    }

    //当たったときに呼ばれる関数
    void OnCollisionEnter(Collision collision)
    {
        //弾に当たったとき
        if(collision.gameObject.tag == "Bullet")
        {
            //弾が消える
            Destroy(collision.gameObject);
            //EnemyのHpが減る
            Hp -= 10;
        }

        if(collision.gameObject.tag == "Ground")
        {
            isGround = true;
        }
    }

    void OnTriggerStay(Collider Enemy)
    {
        if(LayerMask.LayerToName(Enemy.gameObject.layer) == "Cube")
        {
            hantei = true;
        }
    }
    void OnTriggerExit(Collider Enemy)
    {
        if(LayerMask.LayerToName(Enemy.gameObject.layer) == "Cube")
        {
            hantei = false;
        }
    }

    
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
    public float speed = 3.0f; //歩いているときの速度
    public float speed2 = 10.0f; //飛んでいるときの速度
    // bullet prefab
    public GameObject bullet;
    // 弾丸発射点
    public Transform muzzle;
    // 弾丸の速度
    public float dspeed = 1000;
    private Rigidbody rb;  //リジッドボディを取得するための変数
    private float upForce = 5.0f; //上方向にかける力
    private bool isGround; //着地しているかどうかの判定

    private float countup = 0.0f;   //カウントアップ

    public float timelimit = 5.0f;    //タイムリミット

    //Characterのステータス
    private int Hp = 100;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>(); //リジッドボディを取得
    }

    // Update is called once per frame
    void Update()
    {
        if(isGround == true)
        {
            if (Input.GetKey(KeyCode.W))
            {
                transform.position += transform.forward * speed * Time.deltaTime;
            }
            if (Input.GetKey(KeyCode.S))
            {
                transform.position -= transform.forward * speed * Time.deltaTime;
            }
            if (Input.GetKey(KeyCode.D))
            {
                transform.position += transform.right * speed * Time.deltaTime;
            }
            if (Input.GetKey(KeyCode.A))
            {
                transform.position -= transform.right * speed * Time.deltaTime;
            }

            upForce = 0;
        }
        else if(isGround == false)
        {
            if (Input.GetKey(KeyCode.W))
            {
                transform.position += transform.forward * speed2 * Time.deltaTime;
            }
            if (Input.GetKey(KeyCode.S))
            {
                transform.position -= transform.forward * speed2 * Time.deltaTime;
            }
            if (Input.GetKey(KeyCode.D))
            {
                transform.position += transform.right * speed2 * Time.deltaTime;
            }
            if (Input.GetKey(KeyCode.A))
            {
                transform.position -= transform.right * speed2 * Time.deltaTime;
            }
        }

        //左矢印キーを押すと左に回転する
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            transform.Rotate(new Vector3(0, -1, 0));
        }

        //右矢印キーを押すと右に回転する
        if (Input.GetKey(KeyCode.RightArrow))
        {
            transform.Rotate(new Vector3(0, 1, 0));
        }


        //if(isGround == true)
        //{
            if(Input.GetKey("space"))
            {
            isGround = false;
            upForce += 5 * Time.deltaTime;
            rb.AddForce(new Vector3(0, upForce, 0));
            }
        //}

        //
        if(upForce == 25.0f)
        {
           upForce -= 5 * Time.deltaTime;
        }

        // z キーが押された時
        if (Input.GetMouseButtonDown(0))
        {
            // 弾丸の複製
            GameObject bullets = Instantiate(bullet) as GameObject;

            Vector3 force;

            force = this.gameObject.transform.forward * dspeed;

            // Rigidbodyに力を加えて発射
            bullets.GetComponent<Rigidbody>().AddForce(force);

            // 弾丸の位置を調整
            bullets.transform.position = muzzle.position;
        }

        countup += Time.deltaTime;    //時間をカウントする

        if(countup >= timelimit)
        {
            //何かしらの処理
            countup = 0;   //初期化
        }
    }

    void OnCollisionEnter(Collision other)
    {
        if(other.gameObject.tag == "Ground")
        {
            isGround = true;
        }
    }
}

#include "Enemy.h"
#include "Engine/Model.h"
#include "Ground.h"
#include "Engine/Input.h"
#include "Engine/SphereCollider.h"

//コンストラクタ
Enemy::Enemy(GameObject * parent)
	:GameObject(parent, "Enemy"), hModel_(-1)
{
}

//デストラクタ
Enemy::~Enemy()
{
}

//初期化
void Enemy::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("Enemy.fbx");
	assert(hModel_ >= 0);

	Model::SetAnimFrame(hModel_, 1, 150, 2);

	//敵をランダムに配置
	transform_.position_.vecX = rand() % 30 - 10;
	transform_.position_.vecY = rand() % 30 - 10;
	transform_.position_.vecZ = rand() % 30 - 10;

	//衝突範囲
	SphereCollider* collision = new SphereCollider(XMVectorSet(0, 0.5, 0, 0), 0.9f);
	AddCollider(collision);
}

//更新
void Enemy::Update()
{
	FitHeightToGround();
}

//高さを地面に合わせる
void Enemy::FitHeightToGround()
{
	//地面に添わせる
	Ground* pGround = (Ground*)FindObject("Ground");    //ステージオブジェクトを探す
	int hGroundModel = pGround->GetModelHandle();    //モデル番号を取得

	RayCastData data;
	data.start.vecX = transform_.position_.vecX;//レイの発射位置
	data.start.vecY = 0.0f;
	data.start.vecZ = transform_.position_.vecZ;
	//////////
	//	data.start = XMVectorSet(transform_.position_.vecX,
	//		0.0f, transform_.position_.vecZ, 0.0);
	///////////

	data.dir = XMVectorSet(0, -1, 0, 0); //レイの方向
	Model::RayCast(hGroundModel, &data); //レイを発射

										 //レイが当たったら
	if (data.hit)
	{
		//その分位置を下げる
		transform_.position_.vecY = -data.dist;
	}
}

//描画
void Enemy::Draw()
{
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);
}

//開放
void Enemy::Release()
{
}

//何かに当たった
void Enemy::OnCollision(GameObject * pTarget)
{
	//弾に当たったら
	if (pTarget->GetObjectName() == "Bullet")
	{
		KillMe();
		pTarget->KillMe();  //敵が消滅する
		
	}



}
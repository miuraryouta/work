#include "PlayScene.h"
#include "Tank.h"
#include "Ground.h"
#include "Engine/Model.h"
#include "Enemy.h"
#include "Engine/SceneManager.h"



//コンストラクタ
PlayScene::PlayScene(GameObject * parent)
	: GameObject(parent, "PlayScene"), hModel_(-1)
{
}

//初期化
void PlayScene::Initialize()
{
	Instantiate<Tank>(this);
	Instantiate<Ground>(this);
	Instantiate<Enemy>(this);

	//敵が出るのを5回繰り返す
	for (int i = 0; i < 4; i++) {
		Instantiate<Enemy>(this);
	}
	
	
}

//更新
void PlayScene::Update()
{
	//自機を探してもういない
	if (FindObject("Tank") == nullptr)
	{
		//ゲームオーバー画面に変わる
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_GameOver);
	}
}

//描画
void PlayScene::Draw()
{
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);
}

//開放
void PlayScene::Release()
{
}
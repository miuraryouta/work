#include "Tank.h"
#include "Engine/Input.h"
#include "Engine/Model.h"
#include "Engine/Camera.h"
#include "Ground.h"
#include "Cannon.h"
#include "Engine/BoxCollider.h"
#include "Engine/SceneManager.h"

//コンストラクタ
Tank::Tank(GameObject * parent)
	:GameObject(parent, "Tank"), hModel_(-1)
{
}

//デストラクタ
Tank::~Tank()
{
}

//初期化
void Tank::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("TankBody.fbx");
	assert(hModel_ >= 0);

	//子供として砲台を追加
	Instantiate<Cannon>(this);

	BoxCollider* collision = new BoxCollider(XMVectorSet(0, 0, 0, 0), XMVectorSet(1, 1, 1, 0));
	AddCollider(collision);

}

//更新
void Tank::Update()
{
	Move();

	FitHeightToGround();

	//敵を探してもういない
	if (FindObject("Enemy") == nullptr)
	{
		//クリア画面に変わる
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_Clear);
	}
}

//移動処理
void Tank::Move()
{
	XMVECTOR move = { 0.0f, 0.0f, 0.1f, 0.0f };

	XMMATRIX mat;
	mat = XMMatrixRotationY(XMConvertToRadians(transform_.rotate_.vecY));
	move = XMVector3TransformCoord(move, mat);

	//Wキーが押されたら
	if (Input::IsKey(DIK_W))
	{
		//奥に移動
		transform_.position_ += move;
	}

	//Aキーが押されたら
	if (Input::IsKey(DIK_A))
	{
		//左に回転
		transform_.rotate_.vecY -= 5.0f;
	}

	//Dキーが押されたら
	if (Input::IsKey(DIK_D))
	{
		//右に回転
		transform_.rotate_.vecY += 5.0f;
	}
	//カメラの移動
	Camera::SetTarget(transform_.position_ + XMVectorSet(0,5,0,0));

	XMVECTOR camVec = { 0.0f, 8.0f, -10.0f };
	camVec = XMVector3TransformCoord(camVec, mat);
	Camera::SetPosition(transform_.position_ + camVec);
}

//高さを地面に合わせる
void Tank::FitHeightToGround()
{
	//地面に添わせる
	Ground* pGround = (Ground*)FindObject("Ground");    //ステージオブジェクトを探す
	int hGroundModel = pGround->GetModelHandle();    //モデル番号を取得

	RayCastData data;
	data.start.vecX = transform_.position_.vecX;//レイの発射位置
	data.start.vecY = 0.0f;
	data.start.vecZ = transform_.position_.vecZ;
	//////////
	//	data.start = XMVectorSet(transform_.position_.vecX,
	//		0.0f, transform_.position_.vecZ, 0.0);
	///////////

	data.dir = XMVectorSet(0, -1, 0, 0); //レイの方向
	Model::RayCast(hGroundModel, &data); //レイを発射

										 //レイが当たったら
	if (data.hit)
	{
		//その分位置を下げる
		transform_.position_.vecY = -data.dist;
	}
}

//描画
void Tank::Draw()
{
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);
}

//開放
void Tank::Release()
{
}

//何かに当たった
void Tank::OnCollision(GameObject * pTarget)
{
	//敵に当たったとき
	if (pTarget->GetObjectName() == "Enemy")
	{
		KillMe();
		pTarget->KillMe();  //自機が消滅する
	}
}
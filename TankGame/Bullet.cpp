#include "Bullet.h"
#include "Engine/Model.h"
#include "Engine/SphereCollider.h"
//コンストラクタ
Bullet::Bullet(GameObject * parent)
	:GameObject(parent, "Bullet"), hModel_(-1), move_({ 0, 0, 0, 0 })
{
}

//デストラクタ
Bullet::~Bullet()
{
}

//初期化
void Bullet::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("Bullet.fbx");
	assert(hModel_ >= 0);

	//衝突範囲
	SphereCollider* collision = new SphereCollider(XMVectorSet(0, 0, 0, 0), 0.7f);
	AddCollider(collision);

	
}

//更新
void Bullet::Update()
{
	//弾が奥に移動する
	transform_.position_ += move_;
	//move_のY軸だけを引く
	move_.vecY -= GRAVITY;

	//弾が下で消える
	if (transform_.position_.vecY <= -100) {
		KillMe();
	}

	
}

//描画
void Bullet::Draw()
{
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);
}

//開放
void Bullet::Release()
{
}

//弾を発射
void Bullet::Shot(XMVECTOR position, XMVECTOR direction)
{
	transform_.position_ = position;
	//正規化してスカラー倍する
	move_ = XMVector3Normalize(direction) * SPEED;
	
	
}
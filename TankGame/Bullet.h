#pragma once
#include "Engine/GameObject.h"

//弾を管理するクラス
class Bullet : public GameObject
{
	const float SPEED = 0.5f;     //速さ
	const float GRAVITY = 0.03f;  //重力
	int hModel_;    //モデル番号
	XMVECTOR move_;
public:
	//コンストラクタ
	Bullet(GameObject* parent);

	//デストラクタ
	~Bullet();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	void Shot(XMVECTOR position, XMVECTOR direction);
};
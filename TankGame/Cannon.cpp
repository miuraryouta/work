#include "Cannon.h"
#include "Engine/Model.h"
#include "Engine/Input.h"
#include "Bullet.h"


//コンストラクタ
Cannon::Cannon(GameObject * parent)
	:GameObject(parent, "Cannon"), hModel_(-1)
{
}

//デストラクタ
Cannon::~Cannon()
{
}

//初期化
void Cannon::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("TankHead.fbx");
	assert(hModel_ >= 0);
}

//更新
void Cannon::Update()
{
	//矢印右キーが押されていたら
	if (Input::IsKey(DIK_RIGHT))
	{
		//右に回転する
		transform_.rotate_.vecY += 5.0f;
	}

	//矢印左キーが押されていたら
	if (Input::IsKey(DIK_LEFT))
	{
		//左に回転する
		transform_.rotate_.vecY -= 5.0f;
	}

	//スペースキーが押されたら
	if (Input::IsKeyDown(DIK_SPACE))
	{
		//弾を発射
		Bullet* pBullet = Instantiate<Bullet>(FindObject("PlayScene"));

		XMVECTOR shotPos = Model::GetBonePosition(hModel_, "ShotPoint");
		XMVECTOR cannonRoot = Model::GetBonePosition(hModel_,"CannonRoot");

		pBullet->Shot(shotPos, shotPos - cannonRoot);
		
	}
}

//描画
void Cannon::Draw()
{
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);
}

//開放
void Cannon::Release()
{
}
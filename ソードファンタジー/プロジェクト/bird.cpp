#include "bird.h"
#include "Sword.h"
#include "Player.h"
#include "FireMagic.h"
#include "Engine/SceneManager.h"
#include "Engine/Model.h"
#include "Engine/BoxCollider.h"
#include "Engine/Input.h"
#include "Ground.h"




//コンストラクタ
bird::bird(GameObject * parent)
	:GameObject(parent, "bird"), hModel_(-1)
{
}

//デストラクタ
bird::~bird()
{
}

//初期化
void bird::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("bird.fbx");
	assert(hModel_ >= 0);

	//初期位置
	transform_.position_.vecX = 0;
	transform_.position_.vecY = 0;
	transform_.position_.vecZ = 25;

	transform_.rotate_.vecY = 180;

	BoxCollider* collision = new BoxCollider(XMVectorSet(0, 1.5, 0, 0), XMVectorSet(1, 1, 1, 0));
	AddCollider(collision);

	time = 4;

	time2 = 1;

	time3 = 10;

	Model::SetAnimFrame(hModel_, 1, 150, 2);
}

//更新
void bird::Update()
{
	Move();     //移動処理

	Special();      //移動処理以外

	
}

void bird::Move()
{
	Player* pPlayer_ = (Player*)FindObject("Player");

	XMVECTOR direction = { 0.0f, 0.0f, 1.0f, 0.0f };

	XMMATRIX mat;
	mat = XMMatrixRotationY(XMConvertToRadians(transform_.rotate_.vecY));
	direction = XMVector3TransformCoord(direction, mat);

	XMVECTOR distance = pPlayer_->GetPosition() - transform_.position_;



	distance = XMVector3Normalize(distance);
	direction = XMVector3Normalize(direction);

	float naiseki = XMVector3Dot(direction, distance).vecX;
	float angle = acos(naiseki);


	angle = XMConvertToDegrees(angle);



	XMVECTOR move_bi = { 0.0f, 0.0f, 0.2f, 0.0f };

	XMMATRIX mat_bi;
	mat_bi = XMMatrixRotationY(XMConvertToRadians(transform_.rotate_.vecY));
	move_bi = XMVector3TransformCoord(move_bi, mat_bi);

	if (VISION_BI >= angle)
	{
		//プレイヤーのいる方向に向かってゆく
		transform_.rotate_.vecY += 0;

		transform_.position_ += move_bi;
	}
	else if (VISION_BI > angle)
	{
		//プレイヤーのほうに回転
		transform_.rotate_.vecY += 2.0f;
	}
	else if (VISION_BI < angle)
	{
		//プレイヤーのほうに回転
		transform_.rotate_.vecY -= 2.0f;
	}
}

void bird::Special()
{
	//HPが0以下になったとき
	if (HP_BI < 1)
	{
		KillMe(); //鳥が死ぬ

		//シーンが変わる
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_GAMECLEAR4);

	}


	XMVECTOR move_bi = { 0.0f, 0.0f, 0.1f, 0.0f };

	XMMATRIX mat_bi;
	mat_bi = XMMatrixRotationY(XMConvertToRadians(transform_.rotate_.vecY));
	move_bi = XMVector3TransformCoord(move_bi, mat_bi);

	if (flag == 1)
	{
		AT_BI = 0;

		transform_.position_ -= move_bi;

		//攻撃した後4秒間攻撃しない
		timeBeginPeriod(1);
		static DWORD lastFpsResetTime = timeGetTime();
		DWORD nowTime = timeGetTime();
		if (nowTime - lastFpsResetTime > 1000)
		{
			while (time == TimerFlag_BI)
			{


				switch (time)
				{
				case 4:TimerFlag_BI = 4; break;
				case 3:TimerFlag_BI = 3; break;
				case 2:TimerFlag_BI = 2; break;
				case 1:TimerFlag_BI = 1; break;
				case 0:TimerFlag_BI = 0;
				}
				time--;
				TimerFlag_BI--;
				break;
			}
			lastFpsResetTime = nowTime;
		}
		if (TimerFlag_BI == 0)
		{
			AT_BI = 15;
			flag = 0;
			time = 4;
			TimerFlag_BI = 4;
		}
	}

	//特殊攻撃
	if (flag3 == 0)
	{
		Rand_BI = rand() % 10;
		flag3 = 1;
	}
	else if (flag3 == 1)
	{
		if (Rand_BI == 1)
		{

		}
		else if (Rand_BI > 1)
		{

		}
	}
}

//描画
void bird::Draw()
{
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);
}

//開放
void bird::Release()
{
}

void bird::OnCollision(GameObject * pTarget)
{
	Sword* pSword_ = (Sword*)FindObject("Sword");
	FireMagic* pFireMagic_ = (FireMagic*)FindObject("FireMagic");
	Player* pPlayer_ = (Player*)FindObject("Player");
	Ground* pGround_ = (Ground*)FindObject("Ground");

	//

	//剣に当たったとき
	if (pTarget->GetObjectName() == "Sword")
	{
		//Eキーを押した
		if (Input::IsKeyDown(DIK_E))
		{
			if (pPlayer_->flag7 != 5)
			{


				pPlayer_->MP_PL += 5;

				if (pPlayer_->flag1 == 0)
				{
					pPlayer_->SP_PL += 5;
				}

				//HPが減る
				HP_BI = HP_BI - pSword_->AT_S;

				
				pPlayer_->flag7 += 1;
				
			}
		}



	}

	//魔法に当たったとき
	if (pTarget->GetObjectName() == "FireMagic")
	{
		HP_BI = HP_BI - pFireMagic_->AT_FM;
		pTarget->KillMe(); //魔法が消える
	}


	XMVECTOR move_bi = { 0.0f, 0.0f, 0.1f, 0.0f };

	XMMATRIX mat_bi;
	mat_bi = XMMatrixRotationY(XMConvertToRadians(transform_.rotate_.vecY));
	move_bi = XMVector3TransformCoord(move_bi, mat_bi);

	//プレイヤーに当たったとき
	if (pTarget->GetObjectName() == "Player")
	{
		transform_.position_ -= move_bi;  //止まる

		if (flag2 == 0)
		{
			flag2 = 1;
		}
		else if (flag2 == 1)
		{
			AT_BI = 0;

			

			//攻撃前1秒間攻撃しない
			timeBeginPeriod(1);
			static DWORD lastFpsResetTime = timeGetTime();
			DWORD nowTime = timeGetTime();
			if (nowTime - lastFpsResetTime > 1000)
			{
				while (time2 == TimerFlag_BI2)
				{


					switch (time)
					{
					case 1:TimerFlag_BI2 = 1; break;
					case 0:TimerFlag_BI2 = 0;
					}
					time2--;
					TimerFlag_BI2--;
					break;
				}
				lastFpsResetTime = nowTime;
			}
			if (TimerFlag_BI2 == 0)
			{
				AT_BI = 15;
				flag2 = 0;
				time2 = 1;
				TimerFlag_BI2 = 1;
			}
		}

		if (flag == 0)
		{
			pPlayer_->HP_PL = pPlayer_->HP_PL - AT_BI;
			flag = 1;
		}


	}

	//に当たったとき
	if (pTarget->GetObjectName() == "Ground")
	{
		transform_.position_ -= move_bi;
	}
}

void bird::CountDown()
{
	time -= 1;

	time2 -= 1;

	time3 -= 1;
}

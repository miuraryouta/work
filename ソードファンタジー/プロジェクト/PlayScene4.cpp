#include "PlayScene4.h"
#include "Player.h"
#include "Ground.h"
#include "bird.h"
#include "HPBar.h"
#include "MPBar.h"
#include "SPBar.h"
#include "STBar.h"
#include "Background.h"
#include "Engine/Audio.h"
#include "Engine/Image.h"

//コンストラクタ
PlayScene4::PlayScene4(GameObject* parent)
	: GameObject(parent, "PlayScene4"), hPict_(-1)
{
}

//初期化
void PlayScene4::Initialize()
{
	Instantiate<Player>(this);
	Instantiate<Ground>(this);
	Instantiate<bird>(this);
	Instantiate<HPBar>(this);
	Instantiate<MPBar>(this);
	Instantiate<SPBar>(this);
	Instantiate<STBar>(this);
	Instantiate<Background>(this);

	//画像データのロード
	//hPict_ = Image::Load("");
	//assert(hPict_ >= 0);
}

//更新
void PlayScene4::Update()
{
}

//描画
void PlayScene4::Draw()
{
	Image::SetTransform(hPict_, transform_);
	Image::Draw(hPict_);
}

//開放
void PlayScene4::Release()
{
}
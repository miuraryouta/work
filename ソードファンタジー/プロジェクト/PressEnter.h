#pragma once
#include "Engine/GameObject.h"

//プレスエンターの画像を管理するクラス
class PressEnter : public GameObject
{
	int hPict_;    //画像番号

public:



	//コンストラクタ
	PressEnter(GameObject* parent);

	//デストラクタ
	~PressEnter();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	void CountDown();

	//フラグ
	int flag_PE = 0;

	int flag_PE2 = 1;

	int flag_PE3 = 0;

	

	//タイム
	int time_PE;

	char TimerFlag_PE = 1;


	XMVECTOR PE1 = { 0.01f, 0.01f, 0.0f, 0.0f };

	
};
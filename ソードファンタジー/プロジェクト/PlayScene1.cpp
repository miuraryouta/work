#include "PlayScene1.h"
#include "Player.h"
#include "Ground.h"
#include "Slime.h"
#include "HPBar.h"
#include "MPBar.h"
#include "SPBar.h"
#include "STBar.h"
#include "Background.h"
#include "Engine/Audio.h"
#include "Engine/Image.h"



//コンストラクタ
PlayScene1::PlayScene1(GameObject * parent)
	: GameObject(parent, "PlayScene1"), hPict_(-1)
{
}

//初期化
void PlayScene1::Initialize()
{
	Instantiate<Player>(this);
	Instantiate<Ground>(this);
	Instantiate<Slime>(this);
	Instantiate<HPBar>(this);
	Instantiate<MPBar>(this);
	Instantiate<SPBar>(this);
	Instantiate<STBar>(this);
	Instantiate<Background>(this);

	//画像データのロード
//	hPict_ = Image::Load("");
//	assert(hPict_ >= 0);


}

//更新
void PlayScene1::Update()
{
	
}

//描画
void PlayScene1::Draw()
{
	Image::SetTransform(hPict_, transform_);
	Image::Draw(hPict_);
}

//開放
void PlayScene1::Release()
{
}

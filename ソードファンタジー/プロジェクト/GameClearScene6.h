#pragma once
#include "Engine/GameObject.h"


//ゲームクリアシーンを管理するクラス
class GameClearScene6 : public GameObject
{
	int hPict_;    //画像番号
public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	GameClearScene6(GameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};
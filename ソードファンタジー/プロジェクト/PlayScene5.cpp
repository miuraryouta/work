#include "PlayScene5.h"
#include "Player.h"
#include "Ground.h"
#include "Minotaur.h"
#include "HPBar.h"
#include "MPBar.h"
#include "SPBar.h"
#include "STBar.h"
#include "Background.h"
#include "Engine/Audio.h"
#include "Engine/Image.h"

//コンストラクタ
PlayScene5::PlayScene5(GameObject * parent)
	: GameObject(parent, "PlayScene5"), hPict_(-1)
{
}

//初期化
void PlayScene5::Initialize()
{
	Instantiate<Player>(this);
	Instantiate<Ground>(this);
	Instantiate<Minotaur>(this);
	Instantiate<HPBar>(this);
	Instantiate<MPBar>(this);
	Instantiate<SPBar>(this);
	Instantiate<STBar>(this);
	Instantiate<Background>(this);

	//画像データのロード
	//hPict_ = Image::Load("");
	//assert(hPict_ >= 0);
}

//更新
void PlayScene5::Update()
{
}

//描画
void PlayScene5::Draw()
{
	Image::SetTransform(hPict_, transform_);
	Image::Draw(hPict_);
}

//開放
void PlayScene5::Release()
{
}

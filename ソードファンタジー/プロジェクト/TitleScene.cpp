#include "TitleScene.h"
#include "Engine/SceneManager.h"
#include "Engine/Input.h"
#include "Engine/Image.h"
#include "PressEnter.h"
#include "Engine/Audio.h"

//コンストラクタ
TitleScene::TitleScene(GameObject * parent)
	: GameObject(parent, "TitleScene"), hPict_(-1)
{
}

//初期化
void TitleScene::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("title2.png");
	assert(hPict_ >= 0);

	transform_.scale_.vecX -= 0.2f;
	transform_.scale_.vecY -= 0.2f;

	Instantiate<PressEnter>(this);
}

//更新
void TitleScene::Update()
{
	//エンターが押されたら
	if (Input::IsKeyDown(DIK_RETURN))
	{
		//プレイシーンに変わる
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_PLAY1);
	}
}

//描画
void TitleScene::Draw()
{
	Image::SetTransform(hPict_, transform_);
	Image::Draw(hPict_);


}

//開放
void TitleScene::Release()
{
}
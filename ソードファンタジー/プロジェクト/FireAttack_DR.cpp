#include "FireAttack_DR.h"
#include "Engine/BoxCollider.h"
#include "Engine/Model.h"
#include "Player.h"
#include "FireattackRange.h"
#include "Engine/Audio.h"

//コンストラクタ
FireAttack_DR::FireAttack_DR(GameObject* parent)
    :GameObject(parent, "FireAttack_DR"), hModel_(-1)
{
}

//デストラクタ
FireAttack_DR::~FireAttack_DR()
{
}

//初期化
void FireAttack_DR::Initialize()
{
    BoxCollider* collision = new BoxCollider(XMVectorSet(0, 0, 0, 0), XMVectorSet(1, 1, 1, 0));
    AddCollider(collision);

    //モデルデータのロード
    hModel_ = Model::Load("FireAttack.fbx");
    assert(hModel_ >= 0);
}

//更新
void FireAttack_DR::Update()
{
    FireAttackRange* pFAR_ = (FireAttackRange*)FindObject("FireAttackRange");

    //炎は攻撃した後消える
    if (flag == 1)
    {
        KillMe();

        flag = 0;
    }
}

//描画
void FireAttack_DR::Draw()
{
    Model::SetTransform(hModel_, transform_);
    Model::Draw(hModel_);
}

//開放
void FireAttack_DR::Release()
{
}

//何かに当たった
void FireAttack_DR::OnCollision(GameObject* pTarget)
{
    Player* pPlayer_ = (Player*)FindObject("Player");

    //プレイヤーに当たったとき
    if (pTarget->GetObjectName() == "Player")
    {
        pPlayer_->HP_PL = pPlayer_->HP_PL - AT_FA;
    }

}
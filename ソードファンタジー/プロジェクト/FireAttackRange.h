#pragma once
#include "Engine/GameObject.h"

//炎攻撃の範囲（ドラゴン）を管理するクラス
class FireAttackRange : public GameObject
{




public:
    //コンストラクタ
    FireAttackRange(GameObject* parent);

    //デストラクタ
    ~FireAttackRange();

    //初期化
    void Initialize() override;

    //更新
    void Update() override;

    //描画
    void Draw() override;

    //開放
    void Release() override;

    //何かに当たった
    //引数：pTarget 当たった相手
    void OnCollision(GameObject* pTarget) override;

    void CountDown();

    //炎攻撃のフラグ
    int flag_FAR = 0;

    //乱数
    int Rand_FAR;

    int time;

    int flag = 0;

    char TimerFlag_ = 10;
};